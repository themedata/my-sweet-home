+++
date = "2019-01-09T00:00:00Z"
description = "The sample webpage of My Sweet Home theme for Go and Talk page builder."
draft = false
header_class = "bg-transparent"
icon_mask = "square"
inner_icon = "paint-roller"
inner_icon_transform = "shrink-6"
iscjklanguage = false
lastmod = "2019-01-09T00:00:00Z"
lightness = 0.0
lightness_dark = 0.0
lightness_light = 0.0
lightness_lighter = 0.0
logo_url = "/images/logos/icon-on-dark.svg"
main_menu_align = "start"
primary_hue = 0.0
publishdate = "2019-01-09T00:00:00Z"
saturation = 0.0
skip_cover_nav = false
skip_slide_animation = false
title = "About My Sweet Home"
type = "presentation"

[[gmgt_section]]
id = "seuu7gn6a"
partial = "goandtalk/cover/default"

  [gmgt_section.data]
  content = ""
  title = "New Section"

    [gmgt_section.data.appearance]
    section_background = "bg-primary-color-dark"
    trigger_class = "slide-trigger"

[[gmgt_section]]
id = "7ea43"
partial = "goandtalk/content/text-block"

  [gmgt_section.data]
  card_font_size = 4.0
  card_font_weight = "4"
  card_overlay = 0.0
  card_width = 100.0
  embed_aspect_ratio = "16x9"
  height_reference = "h"
  image_br = "br0"
  image_column_width = 100.0
  image_height = 100.0
  image_layer = "absolute"
  image_opacity = 100.0
  image_order = "even-order-1-ns"
  image_under_text = false
  object_fit_class = "of-scale-down"
  quote_display = "dn"
  title_align = "tc"
  title_font_size = "2"
  title_font_weight = "7"
  title_ph = "0"
  title_pv = "3"
  title_transform = "ttc"

    [gmgt_section.data.appearance]
    section_background = "bg-white-60"
    slide_animation = "fadeIn"

    [gmgt_section.data.base_image]
    responsive = false

    [gmgt_section.data.bg_icon]
    bg_icon_display = "dn"
    icon_mask = "square"
    inner_icon = "font"
    inner_icon_transform = "shrink-6"

    [gmgt_section.data.cta]
    align = "center"
    background_color = "bg-primary-color"
    block_button_width = 50.0
    border_color = "b--light-gray"
    border_radius = "br0"
    button_font_size = "4"
    button_text_color = "white-80"
    icon_mask = "coffee"
    inner_icon = "th-large"
    inner_icon_transform = "shrink-10 up-2 left-1"
    mv = 3.0
    name = "Tutorials"
    ph = 3.0
    pv = 3.0
    url = "https://builder.goandmake.app/tutorial/"

    [gmgt_section.data.image]
    responsive = false

    [gmgt_section.data.mobile]

    [gmgt_section.data.base_mobile]

[[gmgt_section]]
data = ""
id = "gt_footer"
is_global = true
partial = "goandtalk/footer/default_footer"
+++

My Sweet Home theme is a theme to build a website quickly to showcase media rich content such as home photos.

Repo for this theme: [https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io](https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io)

## How to Use This Theme

### Hosting With Github Pages (non-profit)

* create an organization on Github
* [import/fork repository](https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io) to this organization, and rename repository to [orgname].github.io

### Hosting With Gitlab Pages

* create a public group on Gitlab
* [fork/import repository](https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io) to this group, and rename repository to [groupname].gitlab.io
* run a default pipeline to publish to Gitlab Pages

### Custom Hosting

* [download](https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io/-/archive/master/my-sweet-home.gitlab.io-master.zip) as a compressed file
* upload to your web server and uncompress

### Customize Website with Go and Talk Page Builder

* navigate to a page, click on edit link at the bottom of the page to open page builder
* edit in the browser, add or remove sections as required
* save webpage to website repository or custom web server to publish
* refer to [demos](https://builder.goandmake.app/demo/) or [tutorials](https://builder.goandmake.app/tutorial/) for details of using the page builder


## license

### theme and template

[Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) ](https://creativecommons.org/licenses/by-nc/4.0/)

A [subscription](https://www.patreon.com/goandmake) is required to remove credit link or to use the template for commercial use.

### Go and Talk Page Builder

Go and Talk page builder is free for non-commercial use.
A [subscription](https://www.patreon.com/goandmake) is required for commercial use.

### Bulk License

A bulk license is available for design agencies to use the page builder on client websites. Please send an email to info at goandmake.app for details.
