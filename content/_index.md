+++
date = "2019-01-09T00:00:00Z"
description = "The sample webpage for My Sweet Home theme."
draft = false
header_class = "bg-transparent"
icon_mask = "square"
inner_icon = "paint-roller"
inner_icon_transform = "shrink-6"
iscjklanguage = false
lastmod = "2019-03-02T16:07:09.133Z"
lightness = 0.0
lightness_dark = 0.0
lightness_light = 0.0
lightness_lighter = 0.0
logo_url = "/images/logos/icon-on-dark.svg"
main_menu_align = "start"
primary_hue = 0.0
publishdate = "2019-01-09T00:00:00Z"
saturation = 43.0
skip_cover_nav = false
skip_slide_animation = false
title = "My Sweet Home theme demo"
type = "presentation"

[[gmgt_section]]
id = "s1zyudvvd"
partial = "goandtalk/cover/two-columns-text-and-image-with-nav"

  [gmgt_section.data]
  card_font_size = 3.0
  card_font_weight = "4"
  card_overlay = 0.0
  card_text_align = "tc"
  card_width = 100.0
  content = "Green living"
  height_reference = "h"
  image_align_h = "left--50-ns"
  image_br = "br0"
  image_column_width = 0.0
  image_height = 100.0
  image_layer = "static"
  image_opacity = 100.0
  image_order = "even-order-1-ns"
  image_under_text = false
  intro = "Page builder Theme Demo"
  object_fit_class = "of-cover"
  quote_display = "dn"
  section_max_width = "mw-100"
  text_column_class = "w-100-ns"
  title = "Home Sweet Home"
  title_align = "tc"
  title_color = "white-90"
  title_font_family = "handwriting"
  title_font_size = -5.0
  title_font_weight = "7"
  title_i = "fs-normal"
  title_ph = "0"
  title_pv = "3"
  title_transform = "ttc"

    [gmgt_section.data.appearance]
    section_background = "bg-primary-color"
    text_color = "white-80"
    trigger_class = "slide-trigger"

    [gmgt_section.data.base_image]
    responsive = false

    [gmgt_section.data.base_mobile]

    [gmgt_section.data.bg_icon]
    bg_icon_display = "dn"
    icon_mask = "square"
    inner_icon = "font"
    inner_icon_transform = "shrink-6"

    [gmgt_section.data.cta]
    align = "center"
    background_color = "bg-primary-color"
    block_button_width = 30.0
    border_color = "b--light-gray"
    border_radius = "br0"
    button_font_size = "4"
    button_text_color = "white-80"
    icon_mask = "coffee"
    icon_order = "dn"
    inner_icon = "th-large"
    inner_icon_transform = "shrink-10 up-2 left-1"
    mv = 3.0
    name = "Inquire"
    ph = 3.0
    pv = 2.0
    url = "#"

    [gmgt_section.data.image]
    animation = "none"
    overlay_background = "bg-black-20"
    responsive = false
    src = "/images/architecture-1867187_1920_pixabay.jpg"

    [gmgt_section.data.mobile]

    [gmgt_section.data.menu]

      [[gmgt_section.data.menu.global]]
      url = "/"
      name = "Home"
      menu = "global"
      identifier = "tutorials"
      pre = ""
      post = ""
      weight = 1.0
      parent = ""

      [[gmgt_section.data.menu.global]]
      url = "/about/"
      name = "About"
      menu = "global"
      identifier = "about"
      pre = ""
      post = ""
      weight = 10.0
      parent = ""

      [[gmgt_section.data.menu.iconmenu]]
      url = "https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io"
      name = "Fork Website Repo"
      menu = "iconmenu"
      identifier = ""
      pre = "Fork Me"
      post = ""
      weight = 1.0
      parent = ""

[[gmgt_section]]
id = "s0ip5lwre"
partial = "goandtalk/slides/collection-big-icon-mono"

  [gmgt_section.data]
  title = "Services"
  content = ""
  intro = " "
  description = "  "
  column_class = "w-33-l"
  resize_column = "flex-none"
  horizontal_position = "justify-center"
  is_global = false

    [[gmgt_section.data.column]]
    icon = "drafting-compass"
    title = "Design"

    [[gmgt_section.data.column]]
    icon = "truck-loading"
    title = "Deliver"

    [[gmgt_section.data.column]]
    icon = "screwdriver"
    title = "Deploy"

    [gmgt_section.data.appearance]
    section_background = "bg-white-90"
    card_background = "hover-primary-color"
    text_color = "primary-color-first-letter-h3 black-30"
    icon_color = "black-20"
    slide_animation = "fadeIn"

[[gmgt_section]]
id = "std9l9mo4"
partial = "goandtalk/slides/alternate-two-columns-text-and-image"

  [gmgt_section.data]
  card_align_h = "justify-center"
  card_font_size = 3.0
  card_font_weight = "4"
  card_overlay = 0.0
  card_text_align = "tl"
  card_width = 100.0
  content = " "
  cta_display = "dn"
  height_reference = "h"
  image_align_h = "left--25-ns"
  image_br = "br0"
  image_column_width = 50.0
  image_height = 100.0
  image_layer = "static"
  image_opacity = 80.0
  image_order = "odd-order-1-ns"
  image_source = "https://source.unsplash.com/jn7uVeCdf6U/600x400"
  image_under_text = false
  intro = "Sun filled living room"
  is_global = false
  object_fit_class = "of-cover"
  quote_display = "dn"
  section_max_width = "mw-100"
  text_column_class = "w-50-ns"
  title = "Natural Lighting"
  title_align = "tc"
  title_font_size = 1.0
  title_font_weight = "7"
  title_ph = "0"
  title_pv = "5"
  title_transform = "ttc"

    [gmgt_section.data.appearance]
    icon_color = "gold"
    section_background = "bg-primary-color-light"
    slide_animation = "fadeIn"

    [gmgt_section.data.base_image]
    responsive = false

    [gmgt_section.data.base_mobile]

    [gmgt_section.data.bg_icon]
    bg_icon_display = "dn"
    icon_mask = "square"
    inner_icon = "font"
    inner_icon_transform = "shrink-6"

    [gmgt_section.data.cta]
    align = "center"
    background_color = "bg-light-gray"
    block_button_width = 50.0
    border_color = "b--light-gray"
    border_radius = "br0"
    button_font_size = "4"
    button_text_color = "mid-gray"
    icon_mask = "circle"
    inner_icon = "info"
    inner_icon_transform = "shrink-4"
    mv = 3.0
    name = " Quick Start "
    ph = 3.0
    pv = 2.0
    url = "#"

    [gmgt_section.data.image]
    overlay_background = "bg-transparent"
    responsive = false

    [gmgt_section.data.mobile]

[[gmgt_section]]
id = "scaw3n0n7"
partial = "goandtalk/slides/alternate-two-columns-text-and-image"

  [gmgt_section.data]
  card_font_size = 3.0
  card_font_weight = "4"
  card_overlay = 0.0
  card_text_align = "tl"
  card_width = 100.0
  content = " "
  cta_display = "dn"
  height_reference = "h"
  image_align_h = "left--25-ns"
  image_br = "br0"
  image_column_width = 50.0
  image_height = 100.0
  image_layer = "static"
  image_opacity = 90.0
  image_order = "odd-order-1-ns"
  image_source = "https://source.unsplash.com/R-LK3sqLiBw/600x400"
  image_under_text = false
  intro = "Dimmable"
  is_global = false
  object_fit_class = "of-cover"
  quote_display = "dn"
  section_max_width = "mw-100"
  text_column_class = "w-50-ns"
  title = "LED Downlights"
  title_align = "tc"
  title_font_size = 1.0
  title_font_weight = "7"
  title_ph = "0"
  title_pv = "5"
  title_transform = "ttc"

    [gmgt_section.data.appearance]
    icon_color = "gold"
    section_background = "bg-primary-color-light"
    slide_animation = "fadeIn"

    [gmgt_section.data.base_image]
    responsive = false

    [gmgt_section.data.base_mobile]

    [gmgt_section.data.bg_icon]
    bg_icon_display = "dn"
    icon_mask = "square"
    inner_icon = "font"
    inner_icon_transform = "shrink-6"

    [gmgt_section.data.cta]
    align = "center"
    background_color = "bg-light-gray"
    block_button_width = 50.0
    border_color = "b--light-gray"
    border_radius = "br0"
    button_font_size = "4"
    button_text_color = "mid-gray"
    icon_mask = "circle"
    inner_icon = "info"
    inner_icon_transform = "shrink-4"
    mv = 3.0
    name = " Quick Start "
    ph = 3.0
    pv = 2.0
    url = "#"

    [gmgt_section.data.image]
    overlay_background = "bg-transparent"
    responsive = false

    [gmgt_section.data.mobile]

[[gmgt_section]]
id = "sbf2v9xh4"
partial = "goandtalk/slides/collection-card"

  [gmgt_section.data]
  column_class = "w-33-l"
  content = ""
  description = "  "
  horizontal_position = "justify-center"
  images_ar = "aspect-ratio--3x4"
  images_border = "bn"
  images_border_color = "b--inherit"
  images_class = "br0"
  images_padding = "pa0"
  intro = " "
  is_global = false
  resize_column = "flex-auto"
  title = "Welcoming"

    [gmgt_section.data.appearance]
    card_background = "hover-primary-color"
    section_background = "bg-white-90"
    slide_animation = "fadeIn"
    text_color = "black-30"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/UV81E0oXXWQ/300x400"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/Dl6H4_nzBpE/300x400"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/jWU9FpLW7fI/300x400"

[[gmgt_section]]
id = "srma1y776"
partial = "goandtalk/slides/collection-card"

  [gmgt_section.data]
  column_class = "w-50-l"
  content = ""
  description = "  "
  horizontal_position = "justify-center"
  images_ar = "aspect-ratio--4x3"
  images_border = "bn"
  images_border_color = "b--inherit"
  images_class = "br0"
  images_padding = "pa0"
  intro = " "
  is_global = false
  resize_column = "flex-none"
  title = "Cozy"

    [gmgt_section.data.appearance]
    card_background = "hover-primary-color"
    section_background = "bg-white-80"
    slide_animation = "fadeIn"
    text_color = "black-30 tc"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/ISBdbfITzws/400x300"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com//rz2YF0vBsvA/400x300"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/ayzkAUNfsDI/400x300"

    [[gmgt_section.data.column]]
    image_source = "https://source.unsplash.com/Y0vcofLCZac/400x300"

[[gmgt_section]]
id = "swruhv6v0"
partial = "goandtalk/slides/iframe-full-width"

  [gmgt_section.data]
  content = ""
  embed_source = "https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d55253.57558682772!2d151.20242374059808!3d-33.8736412970612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sau!4v1549676899675"
  is_global = false
  section_height = "vh-50"
  title = "New Section"

    [gmgt_section.data.appearance]
    icon_color = "gold"
    section_background = "bg-primary-color-dark"
    slide_animation = "fadeIn"

[[gmgt_section]]
id = "saupzqagz"
partial = "goandtalk/content/text-block"

  [gmgt_section.data]
  card_font_size = "5"
  card_font_weight = "4"
  card_overlay = 0.0
  card_width = 100.0
  cta_display = "dn"
  height_reference = "h"
  image_br = "br0"
  image_column_width = 100.0
  image_height = 100.0
  image_layer = "absolute"
  image_opacity = 100.0
  image_order = "even-order-1-ns"
  image_under_text = false
  object_fit_class = "of-scale-down"
  quote_display = "dn"
  title_align = "tc"
  title_font_size = "2"
  title_font_weight = "7"
  title_ph = "0"
  title_pv = "3"
  title_transform = "ttc"
  title = "My Sweet Home theme demo"
  content = "Some descriptions here.\n\n## Make an Appointment\n\n### Business Hours\n7am - 3pm Monday to Friday\n\n### Phone\n02 1111 1111\n\n### Address\nSydney CBD, New South Wales, Australia\n"

    [gmgt_section.data.appearance]
    section_background = "bg-primary-color"
    slide_animation = "fadeIn"

    [gmgt_section.data.base_image]
    responsive = false

    [gmgt_section.data.base_mobile]

    [gmgt_section.data.bg_icon]
    bg_icon_display = "dn"
    icon_mask = "square"
    inner_icon = "font"
    inner_icon_transform = "shrink-6"

    [gmgt_section.data.cta]
    align = "center"
    background_color = "bg-light-gray"
    block_button_width = 50.0
    border_color = "b--light-gray"
    border_radius = "br0"
    button_font_size = "4"
    button_text_color = "mid-gray"
    icon_mask = "circle"
    inner_icon = "info"
    inner_icon_transform = "shrink-4"
    mv = 3.0
    name = " Quick Start "
    ph = 3.0
    pv = 2.0
    url = "#"

    [gmgt_section.data.image]
    overlay_background = "bg-black-05"
    responsive = true
    src = "https://source.unsplash.com/hIJJVnDZQOg/1366x768"
    unsplash_image_id = "hIJJVnDZQOg"

    [gmgt_section.data.mobile]

[[gmgt_section]]
id = "gt_footer"
partial = "goandtalk/footer/default_footer"

  [gmgt_section.data]
  copyright = "Copyright 2019. All rights reserved. "
  is_global = false

    [gmgt_section.data.appearance]
    legal_link_color = "link-white-80"
    nav_link_color = "link-gold"
    section_background = "bg-black-80"
    text_color = "white-80"

    [gmgt_section.data.menu]

      [[gmgt_section.data.menu.footer_legal]]
      name = "Terms"
      url = "#"

      [[gmgt_section.data.menu.footer_legal]]
      name = "Privacy Policy"
      url = "#"

      [[gmgt_section.data.menu.footer_nav]]
      name = "Home"
      url = "#top"
+++

Some descriptions here.

## Make an Appointment

### Business Hours
7am - 3pm Monday to Friday

### Phone
02 1111 1111

### Address
Sydney CBD, New South Wales, Australia
